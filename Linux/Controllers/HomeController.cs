﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Linux.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        
        public ActionResult Login(string email, string password)
        {
            if(email==null && password==null)
                 return View();

            return View("Terminal");
      
        }

        
         
        public ActionResult Register(string username,string email, string password,string confirmpassword,string phone)
        {
            ViewBag.Message = "Your Register page.";
            return View();

        }

        public ActionResult Terminal(string terminal)
        {
            return View();
        }
        
        public ActionResult Otp()
        {
            return View();
        }
    }
}